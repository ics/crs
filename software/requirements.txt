# Packages for Acados solver generation
numpy==1.24.4
PyYAML==6.0.2
oyaml==0.9
rospkg==1.2.3
casadi==3.6.7

# Other generic scientific computation
scipy==1.10.1

# Notebook support
jupyter==1.1.1

# Plotting tools
matplotlib==3.7.5
matplotlib2tikz==0.7.6
cffi==1.14.0

# Coat MPC packages
botorch==0.6.4
GPy==1.10.0
gpytorch==1.6.0
sobol-seq==0.2.0
tikzplotlib==0.8.5
torch==1.10.1

# Model Unit Tests
sympy==1.12

# Pre-Commit
pre-commit

# Evaluatin scripts
bagpy==0.5
