type: "discrete_ekf"
pub_rate: 200.0 # Hz
measurement_timeout_threshold: 2.0 # seconds
max_callback_rate: 200.0  # Hz
log_diagnostic_data: true

# initial state
initial_state:
  type: "pacejka_car"
  value: [0.15, -1.05, 0, 0.7, 0, 0]

initial_input:
  type: "pacejka_car"
  value: [0.0, 0.2]

# initial P (state covariance matrix)
P_init:
  value: [
      [0.1, 0, 0, 0, 0, 0],
      [0, 0.1, 0, 0, 0, 0],
      [0, 0, 0.1, 0, 0, 0],
      [0, 0, 0, 1, 0, 0],
      [0, 0, 0, 0, 1, 0],
      [0, 0, 0, 0, 0, 0.1],
    ]
  is_diag: false

# ============ SENSORS ============
sensors:
  sensor_names: ["mocap", "imu_yaw_rate", "wheel_encoders"] # options: ["mocap", "imu_yaw_rate", "imu", "wheel_encoder", "lighthouse"]
  mocap:
    R:
      value: [[0.001],[0.001],[0.001]]
      is_diag: true
    key: mocap # optional, if not set use name of sensor
    outlier_rejection:
      use_outlier_rejection: true
      outlier_rejection_type: "cov_threshold"
      outlier_threshold: 3
      max_consecutive_outliers: 5

  imu_yaw_rate:
    R:
      value: [[0.1]]
      is_diag: true
    key: imu_yaw_rate # optional, if not set use name of sensor
    outlier_rejection:
      use_outlier_rejection: true
      outlier_rejection_type: "cov_threshold"
      outlier_threshold: 5
      max_consecutive_outliers: 8

  wheel_encoders:
    R:
      value: [[1], [1], [1], [1]]
      is_diag: true
    key: wheel_encoders # optional, if not set use name of sensor
    outlier_rejection:
      use_outlier_rejection: true
      outlier_rejection_type: "cov_threshold"
      outlier_threshold: 5
      max_consecutive_outliers: 8

  lighthouse:
    sensor_pos:
      value: [[0.035, 0.02, 0.035, 0.02], [0.015, 0.015, -0.015, -0.015]]
    key: lighthouse
    base_stations: ["bs0"]
    bs0:
      bs_ID: 0
      R:
        value: [[0.0000101, 0.00001, 0.00001, 0.00001],
                 [0.00001, 0.0000101, 0.00001, 0.00001],
                 [0.00001, 0.00001, 0.0000101, 0.00001],
                 [0.00001, 0.00001, 0.00001, 0.0000101]]
      P_bs:
        value: [[0.423652], [0.449405], [1.99268]]
      R_bs:
        value: [[-0.0939952, 0.993122, -0.0698045], [-0.0609725, -0.0757257, -0.995263], [-0.993704, -0.0892937, 0.067671]]
      dt1: -0.0508728
      dt2: 0.0464478
    outlier_rejection:
      use_outlier_rejection: true
      outlier_rejection_type: "cov_threshold"
      outlier_threshold: 3
      max_consecutive_outliers: 8

  # unified_lighthouse:
  #   sensor_pos:
  #     value: [[0.035, 0.02, 0.035, 0.02], [0.015, 0.015, -0.015, -0.015]]
  #   key: unified_lighthouse
  #   base_stations: ["bs0"]
  #   bs0:
  #     bs_ID: 0
  #     R:
  #       value: [[0.0000101, 0.00001, 0.00001, 0.00001, 0, 0, 0, 0],
  #                [0.00001, 0.0000101, 0.00001, 0.00001, 0, 0, 0, 0],
  #                [0.00001, 0.00001, 0.0000101, 0.00001, 0, 0, 0, 0],
  #                [0.00001, 0.00001, 0.00001, 0.0000101, 0, 0, 0, 0],
  #                [0, 0, 0, 0, 0.0000101, 0.00001, 0.00001, 0.00001],
  #                [0, 0, 0, 0, 0.00001, 0.0000101, 0.00001, 0.00001],
  #                [0, 0, 0, 0, 0.00001, 0.00001, 0.0000101, 0.00001],
  #                [0, 0, 0, 0, 0.00001, 0.00001, 0.00001, 0.0000101]]
  #     P_bs:
  #       value: [[0.423652], [0.449405], [1.99268]]
  #     R_bs:
  #       value: [[-0.0939952, 0.993122, -0.0698045], [-0.0609725, -0.0757257, -0.995263], [-0.993704, -0.0892937, 0.067671]]
  #     dt1: -0.0508728
  #     dt2: 0.0464478
  #   outlier_rejection:
  #     use_outlier_rejection: true
  #     outlier_rejection_type: "cov_threshold"
  #     outlier_threshold: 3

# ============ VISUALIZER ============
visualizer:
  # car_ekf_visualizer will plot the covariance of the position estimate as a ellipsoid
  type: car_ekf_visualizer

  # === Parameters for Base Visualizer ===
  rate: 10
  # Visualizer specific  parameters
  frame_id: crs_frame # default
  namespace: ekf # default

  use_arrows: true # If true, use arrows to visualize planned and reference yaw angle. This option is a lot slower and may introduce visual lags

  # Color of the estimated position
  est_r: 0
  est_g: 0
  est_b: 0
  est_a: 1

  size_x: 0.05 # 0.07 for arrows
  size_y: 0.05 # 0.02 for arrows
  size_z: 0.05 # 0.02 for arrows

  # ==== Special Parameters for ekf visualizer ====

  # Color of the covariance ellipsoid
  cov_r: 0
  cov_g: 0
  cov_b: 1
  cov_a: 0.3

  cov_scale: 100 # scales covariance ellipsoid by 100

# ============ THIS IS ENTIRELY OPTIONAL ============
# If this is not defiend, then the default model which is loaded, is from the ros paramaters located at /model
# This means that e.g. the EKF uses the same Q as the simulation -> EKF has perfect model and Q

model:
  # type describes what type of model we want to use / should be loaded
  type: "pacejka_discrete"

  # Q (process noise covariance matrix)
  Q:
    value:
      [
        [0.01, 0, 0, 0, 0, 0],
        [0, 0.01, 0, 0, 0, 0],
        [0, 0, 0.01, 0, 0, 0],
        [0, 0, 0, 0.001, 0, 0],
        [0, 0, 0, 0, 0.001, 0],
        [0, 0, 0, 0, 0, 0.001],
      ]
    is_diag: false

#  model_params: # only change size and inertia of model
#    m: 0.200
#    I: 0.000605
