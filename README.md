# Chronos and CRS

Welcome to the Chronos and CRS repository, an affordable hardware platform and advanced software for control and robotics research and education.

### What is Chronos?

Chronos is a miniature, 1/28th scale car-like robot designed to lower the barrier of entry for control and robotics experiments. With customized open-source electronics and firmware, Chronos offers a low-cost yet agile platform suitable for both educational projects and cutting-edge research.


### What is CRS?

CRS stands for Control and Robotics Software and provides an advanced control software framework to support simulations and experiments in the fields of control and robotics. Our software packages include optimization-based system identification, state estimation, and control algorithms, empowering researchers and students to explore complex control strategies.


## Get Started

The repository is organized into three main folders:
- **Hardware**: Contains electronic schematics, PCB designs, and assembly instructions for the Chronos hardware platform.
- **Firmware**: Includes firmware code for the microcontrollers used in Chronos, enabling communication and control of the hardware components.
- **Software**: Hosts the control software framework CRS, including algorithms for system identification, state estimation, and control.
- **Datasets**: Contains a pointer to the datasets used for nonlinear system identification and validation of the state estimators. 


Visit the [Wiki](https://gitlab.ethz.ch/ics/crs/-/wikis/home) to learn more on how to start with Chronos and CRS.


## Citing 

If you use Chronos and/or CRS please cite:

> [Andrea Carron, Sabrina Bodmer, Lukas Vogel,  Ren&eacute; Zurbr&uuml;ugg, David Helm, Rahel Rickenbach,  Simon Muntwiler, Jerome Sieber and  Melanie N. Zeilinger, "Chronos and CRS: Design of a miniature car-like robot and a software framework for single and multi-agent robotics and control," 2023 IEEE International Conference on Robotics and Automation (ICRA), London, United Kingdom, 2023, pp. 1371-1378](https://ieeexplore.ieee.org/abstract/document/10161434)

> [Sabrina Bodmer, Lukas Vogel, Simon Muntwiler, Alexander Hansson, Tobias Bodewig, Jonas Wahlen, Melanie N. Zeilinger, and Andrea Carron, "Optimization-Based System Identification and Moving Horizon Estimation Using Low-Cost Sensors for a Miniature Car-Like Robot", ArXiv 2024](https://arxiv.org/pdf/2404.08362)

---

