#ifndef SRC_CRS_PLANNING_COMMON_INCLUDE_PLANNING_CARTESIAN_REFERENCE_POINT
#define SRC_CRS_PLANNING_COMMON_INCLUDE_PLANNING_CARTESIAN_REFERENCE_POINT

namespace crs_planning
{

struct cartesian_reference_point
{
  double x;
  double y;
};
}  // namespace crs_planning
#endif /* SRC_CRS_PLANNING_COMMON_INCLUDE_PLANNING_CARTESIAN_REFERENCE_POINT */
