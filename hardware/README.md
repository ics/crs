# Chronos Car Hardware Guide
[![License](https://img.shields.io/badge/License-BSD_2--Clause-orange.svg)](https://opensource.org/licenses/BSD-2-Clause)

This README serves as a quick reference to the assembly guide for the Chronos car. For detailed instructions, diagrams, and resources, please refer to the [assembly guide](https://gitlab.ethz.ch/ics/crs/-/wikis/crs-for-cars/how-to-build-a-chronos-car).

#### What You'll Find in the Assembly Guide:

- **PCBs (Printed Circuit Boards):** Detailed schematics and layouts for all electronic components.
  
- **Gerber Files:** Manufacturing files used for producing PCBs.

- **3D Printed Parts Models:** Models and specifications for all 3D printed components required for assembly.

- **Bill of Materials:** A list of the material needed to assemble Chronos.

#### Need Help?

If you have any questions or encounter difficulties during assembly, don't hesitate to reach us out. The main point of contact is [Andrea Carron](https://idsc.ethz.ch/research-zeilinger/research-projects/demonstrators.html).

Let's get started on building your Chronos car!