{
  "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Optimization-based system identification\n",
    "\n",
    "This notebook contains demonstration code for performing system identification on the all-wheel drive model."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%load_ext autoreload\n",
    "%autoreload 2"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import matplotlib\n",
    "import matplotlib.pyplot as plt\n",
    "import numpy as np\n",
    "import os\n",
    "\n",
    "from opt_sys_id.model.awd_model import AwdBicycleModel, States, Params\n",
    "from opt_sys_id.optimizer import SubtrajectoryOptimizer\n",
    "from opt_sys_id.simulator import Simulator\n",
    "from opt_sys_id.state_estimator import StateEstimator\n",
    "\n",
    "from opt_sys_id.sensor_model import LighthouseSensorModel, WheelEncoderSensorModel, YawRateSensorModel"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Global parameters\n",
    "N = 1500         # Training data set length\n",
    "M = 50           # Subtrajectory length in parameter optimization\n",
    "N_val = 1500     # Validation data set length\n",
    "M_val = 100      # Prediction horizon for validation"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Configurations\n",
    "configs = {}\n",
    "\n",
    "# Specify training and validation data sets\n",
    "train = 'fie_mpc_lighthouse_1'          # refers to a dataset under ../datasets/\n",
    "val = 'fie_mpc_lighthouse_2'            # dito\n",
    "\n",
    "configs[train] = {\n",
    "    'start': 100,\n",
    "    'end' : -100,\n",
    "    'supsampling_rate': 1,\n",
    "    'start_viz': 50,\n",
    "    'end_viz': -50,\n",
    "    'theta0': Params.get_default(),\n",
    "}\n",
    "\n",
    "configs[val] = {\n",
    "    'start': 50,\n",
    "    'end' : -50,\n",
    "    'supsampling_rate': 1,\n",
    "    'start_viz': 50,\n",
    "    'end_viz': -50,\n",
    "    'theta0': Params.get_default(),\n",
    "}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Load all data sets\n",
    "y_full, u_full, t_full, x_full = {}, {}, {}, {}\n",
    "\n",
    "for label, config in configs.items():\n",
    "    directory = os.path.join('../datasets', 'real', label)\n",
    "    start = config[\"start\"]\n",
    "    end = config[\"end\"]\n",
    "    supsampling_rate = config[\"supsampling_rate\"]\n",
    "\n",
    "    y_full[label] = np.load(os.path.join(directory, \"y.npy\"))[\n",
    "        :, start:end:supsampling_rate\n",
    "    ]\n",
    "    u_full[label] = np.load(os.path.join(directory, \"u.npy\"))[\n",
    "        :, start:end:supsampling_rate\n",
    "    ]\n",
    "    t_full[label] = np.load(os.path.join(directory, \"t.npy\"))[\n",
    "        start:end:supsampling_rate\n",
    "    ]\n",
    "    x_full[label] = np.load(os.path.join(directory, \"x.npy\"))[\n",
    "        :, start:end:supsampling_rate\n",
    "    ]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Adjust the training and validation data sets in case not large enough\n",
    "\n",
    "# Training dataset\n",
    "if y_full[train].shape[1] < N:\n",
    "    N = (y_full[train].shape[1] // M) * M\n",
    "    print(f\"Training data set too short, reducing N to {N}.\")\n",
    "\n",
    "# Validation dataset\n",
    "if y_full[val].shape[1] < N_val:\n",
    "    N_val = (y_full[val].shape[1] // M_val) * M_val\n",
    "    print(f\"Validation data set too short, reducing N_val to {N_val}.\")\n",
    "\n",
    "# Crop training and validation data sets\n",
    "y_train = y_full[train][:, : N + 1]\n",
    "u_train = u_full[train][:, :N]\n",
    "t_train = t_full[train][: N + 1]\n",
    "x_train = x_full[train][:, : N + 1]\n",
    "\n",
    "y_val = y_full[val][:, :N_val + 1]\n",
    "u_val = u_full[val][:, : N_val]\n",
    "t_val = t_full[val][ : N_val + 1]\n",
    "x_val = x_full[val][:, : N_val + 1]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Sanity check: visualize the training data\n",
    "\n",
    "Here, we want to first also do a visual check on the data we pass to the optimization routine. Is it sufficiently continuous? Does it have large outliers?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, axs = plt.subplots(1, 2, facecolor='w', figsize=(10, 5))\n",
    "\n",
    "fig.suptitle('Visualization of training data')\n",
    "\n",
    "axs[0].set(title='Components of state estimate vector $x$ over time')\n",
    "lines = axs[0].plot(t_train, x_train.T)\n",
    "axs[0].legend(iter(lines), range(x_train.shape[0]))\n",
    "\n",
    "axs[1].set(title='Components of measurement vector $y$ over time')\n",
    "lines = axs[1].plot(t_train, y_train.T)\n",
    "axs[1].legend(iter(lines), range(y_train.shape[0]))\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Incorporate prior known Lighthouse parameters\n",
    "\n",
    "In this case, we already know the pose of the Lighthouse base station from a prior calibration routine. We fix it in the sensor model."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# (Known) Lighthouse calibration parameters\n",
    "R_bs = np.array(\n",
    "    [[-0.00170188, 0.99949, 0.031886],\n",
    "     [-0.367629, 0.0290278, -0.92952],\n",
    "     [-0.929971, -0.0133041, 0.367392]]\n",
    ")\n",
    "p_bs = np.array([[0.189362], [0.138069], [1.74295]])\n",
    "T_bs = np.eye(4)\n",
    "T_bs[:3, :3] = R_bs\n",
    "T_bs[:3, 3] = p_bs.flatten()\n",
    "\n",
    "dt1 = -0.0508728\n",
    "dt2 = 0.0464478\n",
    "\n",
    "sensor_pos = np.array([[-0.045, -0.06, -0.045, -0.06], [0.015, 0.015, -0.015, -0.015]])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Create the sensor models and dynamic model\n",
    "\n",
    "In the example dataset, the measurement vector consists of the stacked Lighthouse measurements (8 entries) and wheel encoder measurements (4 entries). We create measurement models and let the `PoseAwdBicycleModel` stack them internally. The dynamics are given by `calc_y` of the `PoseAwdBicycleModel`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "lighthouse_model = LighthouseSensorModel(T_bs, sensor_pos, dt1, dt2)\n",
    "wheel_encoder_model = WheelEncoderSensorModel(\n",
    "    Params.wheel_radius, Params.lr, Params.lf, Params.car_width\n",
    ")\n",
    "yaw_model = YawRateSensorModel()\n",
    "\n",
    "# Instantiate model and pass the sensor models. Fix the easily measured car width and mass.\n",
    "model = AwdBicycleModel(States.full(), [lighthouse_model, wheel_encoder_model, yaw_model])\n",
    "fixed_params = [Params.m, Params.car_width]\n",
    "model.fixed_params = fixed_params"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Set prior parameters\n",
    "theta0 = configs[train][\"theta0\"]\n",
    "dt = float(t_train[-1] - t_train[0]) / (len(t_train) - 1)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Create optimizer and state estimator instances\n",
    "solver = SubtrajectoryOptimizer(model, dt, N, M)\n",
    "state_estimator = StateEstimator(model, dt, N_val)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Set the tuning parameters\n",
    "P = np.eye(model.num_params) * 3           # (theta - theta0)^T P (theta - theta0)\n",
    "P[:4,:4] *= 1e1\n",
    "P[-2, -2] *= 1e-2                          # wheel radius absorbs biases in the wheel encoders\n",
    "\n",
    "Q = np.eye(model.num_states) * 5           # Process noise regularization\n",
    "Q[3:, 3:] *= 0.5                           # more process noise on velocities\n",
    "\n",
    "R = np.eye(model.num_measurements) * 100   # Measurement noise regularization\n",
    "R[8:, 8:] *= 1e-8                          # non LH measurements are noisier and have larger magnitudes, both by a factor of ~100\n",
    "\n",
    "S = np.eye(model.num_states) * 1           # Initial state regularization"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "x0 = x_train[:, 0:-1:M]\n",
    "\n",
    "# Set the warm-start\n",
    "xws = x_train\n",
    "xws_estimator = x_val"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Identify the parameters using the training data\n",
    "sol = solver.find_params(y_train, u_train, theta0, P, Q, R, S, x0, t=t_train, xws=xws)\n",
    "theta_hat = solver.theta_hat[:, 0]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Print the prior and posterior parameters\n",
    "\n",
    "print(\"Prior parameters\")\n",
    "Params.print_params(theta0)\n",
    "print(\"--------------------\")\n",
    "print(\"Posterior parameters\")\n",
    "Params.print_params(theta_hat)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Using the both the prior and posterior parameter sets, perform state estimation on the validation data set\n",
    "state_estimator.find_states(y_val, u_val, theta0, Q, R, S, t=t_val, xws=xws_estimator)\n",
    "x_est_prior = state_estimator.x_hat\n",
    "\n",
    "state_estimator.find_states(\n",
    "    y_val, u_val, theta_hat, Q, R, S, t=t_val, xws=xws_estimator\n",
    ")\n",
    "x_est_posterior = state_estimator.x_hat"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Create simulators for prior and posterior parameter sets\n",
    "sim_prior = Simulator(theta0, dt)\n",
    "sim_prior.model = model\n",
    "sim_posterior = Simulator(theta_hat, dt)\n",
    "sim_posterior.model = model"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Make predictions on the validation dataset over a fixed horizon for each parameter set\n",
    "x_prior, x_posterior = [], []\n",
    "\n",
    "for k in range(N_val - M_val):\n",
    "    x0_prior = x_est_prior[:, k]\n",
    "    x0_posterior = x_est_posterior[:, k]\n",
    "    u = u_val[:, k : k + M_val]\n",
    "    t = t_val[k : k + M_val + 1]\n",
    "\n",
    "    x_prior += [sim_prior.simulate(x0_posterior, u, t=t)[0]]\n",
    "    x_posterior += [sim_posterior.simulate(x0_posterior, u, t=t)[0]]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Visualize the predicted trajectories\n",
    "M_viz = M_val - 5\n",
    "alpha = 0.1\n",
    "\n",
    "c_gt = \"#B51963\"\n",
    "c_posterior = \"#0073E6\"\n",
    "c_prior = \"#5BA300\"\n",
    "\n",
    "start_viz = configs[val][\"start_viz\"]\n",
    "end_viz = configs[val][\"start_viz\"] + 400\n",
    "\n",
    "plt.figure(facecolor=\"w\")\n",
    "plt.title(f\"Predictions over {M_viz*dt:.1f} s\")\n",
    "\n",
    "plt.axis([-2.3, 2.3, -1.8, 2.8])\n",
    "plt.gca().set_aspect(\"equal\", adjustable=\"box\")\n",
    "plt.xlabel(\"$x_p$ [m]\")\n",
    "plt.ylabel(\"$y_p$ [m]\")\n",
    "\n",
    "plt.plot([], c=c_gt, label=\"Closed loop estimate\")\n",
    "plt.plot([], c=c_prior, label=\"Prior parameter ($\\\\bar \\\\theta_0$)\")\n",
    "plt.plot([], c=c_posterior, label=\"Posterior parameter ($\\\\hat \\\\theta$)\")\n",
    "plt.legend(loc=\"upper left\")\n",
    "\n",
    "plt.plot(x_val[0, start_viz:end_viz], x_val[1, start_viz:end_viz], c=c_gt, zorder=10, linestyle=\"-\", linewidth=2)\n",
    "\n",
    "for x_posterior_i in x_posterior[start_viz:end_viz]:\n",
    "    plt.plot(\n",
    "        x_posterior_i[0, :M_viz],\n",
    "        x_posterior_i[1, :M_viz],\n",
    "        c=c_posterior,\n",
    "        alpha=alpha,\n",
    "        zorder=5,\n",
    "    )\n",
    "\n",
    "for x_prior_i in x_prior[start_viz:end_viz]:\n",
    "    plt.plot(\n",
    "        x_prior_i[0, :M_viz], x_prior_i[1, :M_viz], c=c_prior, alpha=alpha, zorder=0\n",
    "    )"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Calculate the prediction mean squared errors\n",
    "e_existing, e_proposed = [], []\n",
    "\n",
    "for k in range(N_val - M_val):\n",
    "    e_existing += [x_prior[k] - x_est_posterior[:, k : k + M_val + 1]]\n",
    "    e_proposed += [x_posterior[k] - x_est_posterior[:, k : k + M_val + 1]]\n",
    "\n",
    "mse_existing = np.mean(np.array(e_existing) ** 2)\n",
    "mse_proposed = np.mean(np.array(e_proposed) ** 2)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Print mean squared errors\n",
    "print(f'mse_existing: {mse_existing:.8f}')\n",
    "print(f'mse_proposed: {mse_proposed:.8f}')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Create an animation of the predictions\n",
    "M_viz = 95\n",
    "animation_interval = 17\n",
    "alpha = 0.1\n",
    "\n",
    "c_gt = \"#B51963\"\n",
    "c_posterior = \"#0073E6\"\n",
    "c_prior = \"#5BA300\"\n",
    "\n",
    "start_viz = configs[val][\"start_viz\"]\n",
    "end_viz = configs[val][\"end_viz\"]\n",
    "\n",
    "plt.figure(facecolor=\"w\")\n",
    "plt.title(f\"Predictions over {M_viz*dt:.1f} s\")\n",
    "\n",
    "plt.axis([-2.5, 3.2, -1.7, 3.4])\n",
    "plt.gca().set_aspect(\"equal\", adjustable=\"box\")\n",
    "plt.xlabel(\"$x_p$ [m]\")\n",
    "plt.ylabel(\"$y_p$ [m]\")\n",
    "\n",
    "\n",
    "plt.plot([], c=c_gt, label=\"Closed loop estimate\")\n",
    "plt.plot([], c=c_prior, label=\"Existing Method\")\n",
    "plt.plot([], c=c_posterior, label=\"Proposed Method\")\n",
    "plt.legend(loc=\"upper left\")\n",
    "\n",
    "if end_viz < 0:\n",
    "    end_viz += N_val\n",
    "\n",
    "\n",
    "def animate(i):\n",
    "    for k in range(\n",
    "        i * animation_interval, min((i + 1) * animation_interval, end_viz - start_viz)\n",
    "    ):\n",
    "        plt.plot(x_val[0, :k], x_val[1, :k], c=c_gt, zorder=10)\n",
    "        plt.plot(\n",
    "            x_posterior[k][0, :M_viz],\n",
    "            x_posterior[k][1, :M_viz],\n",
    "            c=c_posterior,\n",
    "            alpha=alpha,\n",
    "            zorder=5,\n",
    "        )\n",
    "        plt.plot(\n",
    "            x_prior[k][0, :M_viz],\n",
    "            x_prior[k][1, :M_viz],\n",
    "            c=c_prior,\n",
    "            alpha=alpha,\n",
    "            zorder=0,\n",
    "        )\n",
    "\n",
    "\n",
    "from matplotlib.animation import PillowWriter, FuncAnimation\n",
    "\n",
    "ani = FuncAnimation(\n",
    "    plt.gcf(),\n",
    "    animate,\n",
    "    frames=int((end_viz - start_viz) / animation_interval + 16),\n",
    "    repeat=False,\n",
    ")\n",
    "ani.save(\"../figures/validation.gif\", dpi=300, writer=PillowWriter(fps=12))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.10"
  },
  "orig_nbformat": 4
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
