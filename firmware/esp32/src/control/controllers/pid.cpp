/*******************************************************************************
 * @file    pid.cpp
 * @brief   Generic PID controller implementation.
 ******************************************************************************/

#include "pid.hpp"

namespace chronos {

namespace control {

// Empty file, required for ESP-IDF to correctly compile this component.

} // namespace control
} // namespace chronos
