# Chronos and CRS Datasets
Together with the Chronos hardware and firmware and the CRS software, we also share the datasets. In the following, you can find links to the datasets and reference to the associated papers


> Amon Lahr, Joshua Näf, Kim P. Wabersich, Jonathan Frey, Pascal Siehl, Andrea Carron, Moritz Diehl, and Melanie N. Zeilinger. "L4acados: Learning-based models for acados, applied to Gaussian process-based predictive control." arXiv preprint arXiv:2411.19258 (2024).

The data associated to the miniature racing experiments published in this paper can be found at this [link](https://www.research-collection.ethz.ch/handle/20.500.11850/707631).

---

> Sabrina Bodmer, Lukas Vogel, Simon Muntwiler, Alexander Hansson, Tobias Bodewig, Jonas Wahlen, Melanie N. Zeilinger, and Andrea Carron, "Optimization-Based System Identification and Moving Horizon Estimation Using Low-Cost Sensors for a Miniature Car-Like Robot", ArXiv 2024 (accepted at CDC24).

These datasets can become a valuable benchmark for nonlinear system identification and estimation. The dataset can be found [here](https://www.research-collection.ethz.ch/handle/20.500.11850/711739). The script to run the system identification algorithm can be found [here](https://gitlab.ethz.ch/ics/crs/-/tree/main/software/src/ros4crs/tools/opt_sys_id?ref_type=heads), and to run the state estimation can be found [here](https://gitlab.ethz.ch/ics/crs/-/tree/main/software/src/scripts/estimator_comparison?ref_type=heads). 

